<?php

namespace Drupal\field_time\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Plugin implementation of the 'time' field type.
 *
 * @FieldType(
 *   category= "date_time",
 *   id = "time_range",
 *   label = @Translation("Time Range « Human »"),
 *   description = @Translation("Time range field, stored for human readbility, e.g. 12:00:00"),
 *   default_widget = "time_range_widget",
 *   default_formatter = "time_range_formatter"
 * )
 */
class TimeRangeType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['from'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Start time'))
      ->setDescription(new TranslatableMarkup('Format HH:MM:SS'))
      ->setSetting('maxlength', 8)
      ->setRequired(TRUE);

    $properties['to'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('End time'))
      ->setDescription(new TranslatableMarkup('Format HH:MM:SS'))
      ->setSetting('maxlength', 8)
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'from' => [
          'type' => 'char',
          'pgsql_type' => 'time',
          'mysql_type' => 'time',
          'length' => 8,
          'not null' => FALSE,
        ],
        'to' => [
          'type' => 'char',
          'pgsql_type' => 'time',
          'mysql_type' => 'time',
          'length' => 8,
          'not null' => FALSE,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['from'] = '10:55:00';
    $values['to'] = '11:55:00';
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $from = $this->get('from')->getValue();
    $to = $this->get('to')->getValue();
    return empty($from) || empty($to) || trim($from) === '' || trim($to) === '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    // Add a custom callback constraint to validate that 'to' time is not earlier than 'from' time.
    $constraints[] = new Callback(['callback' => [$this, 'validateTimeRange']]);

    return $constraints;
  }

  /**
   * Custom validation callback for the time range field.
   */
  public function validateTimeRange($item, ExecutionContextInterface $context, $payload) {
    // Iterate through the field items to validate each one.

    $from_time = strtotime($item->from);
    $to_time = strtotime($item->to);

    // Check if the 'to' time is earlier than the 'from' time.
    if ($to_time <= $from_time) {
      // Add a violation message.
      $context->buildViolation('The end time cannot be earlier or equal to the start time.')
        ->atPath('to')
        ->addViolation();
    }
  }
}
