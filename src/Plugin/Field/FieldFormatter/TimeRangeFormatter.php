<?php

namespace Drupal\field_time\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'time_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "time_range_formatter",
 *   label = @Translation("Time range formatter"),
 *   field_types = {
 *     "time_range"
 *   }
 * )
 */
class TimeRangeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $format = $this->getSetting('time_format');
    $timerange_format = $this->getSetting('timerange_format');

    foreach ($items as $delta => $item) {
      $value = str_replace('start', (new \DateTimeImmutable($item->from))->format($format), $timerange_format);
      $value = str_replace('end', (new \DateTimeImmutable($item->to))->format($format), $value);
      $elements[$delta] = ['#markup' => $value];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['timerange_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time Range Format'),
      '#default_value' => (string) $this->getSetting('timerange_format'),
      '#description' => $this->t('This setting must have `start` and `end` keys'),
    ];

    $elements['time_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time Format'),
      '#default_value' => $this->getSetting('time_format'),
      '#description' => $this->getTimeDescription(),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'time_format' => 'h:i a',
      'timerange_format' => 'start ~ end',
    ] + parent::defaultSettings();
  }

  /**
   * Description of the time field.
   *
   * @return string
   *   Description of the time field.
   */
  private function getTimeDescription() {
    $output = '';
    $output .= $this->t('a - Lowercase am or pm') . '<br/>';
    $output .= $this->t('A - Uppercase AM or PM') . '<br/>';
    $output .= $this->t('B - Swatch Internet time (000 to 999)') . '<br/>';
    $output .= $this->t('g - 12-hour format of an hour (1 to 12)') . '<br/>';
    $output .= $this->t('G - 24-hour format of an hour (0 to 23)') . '<br/>';
    $output .= $this->t('h - 12-hour format of an hour (01 to 12)') . '<br/>';
    $output .= $this->t('H - 24-hour format of an hour (00 to 23)') . '<br/>';
    $output .= $this->t('i - Minutes with leading zeros (00 to 59)') . '<br/>';
    $output .= $this->t('s - Seconds, with leading zeros (00 to 59)') . '<br/>';
    return $output;
  }

}
